<?php include 'konekcija.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Zalihe proizvoda Beograd </title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <link href="favicon.ico" rel="shortcut icon">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate-css/animate.min.css" rel="stylesheet">

  <link href="css/style.css" rel="stylesheet">
</head>

<body>
  <div id="preloader"></div>

  <?php include 'header.php'; ?>

  <section id="about">
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-12">
          <h3 class="section-title">Pregled stanja</h3>
          <div class="section-title-divider"></div>
        </div>
      </div>
    </div>
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-12">

          <table class="table table-hover">
            <thead>
              <tr>
                <th>Prodavnica</th>
                <th>Naziv proizvoda</th>
                <th>Kategorija</th>
                <th>Kolicina</th>
              </tr>
            </thead>
            <tbody>
              <?php

              //$stanje = $db->rawQuery("select * from stanje s join prodavnica p on 
                //s.prodavnicaID=p.prodavnicaID join proizvod pr on s.proizvodID = pr.proizvodID 
                //join kategorija k on pr.kategorijaID=k.kategorijaID");
              $zahtev = curl_init("http://localhost:8080/zaliheSeminarski/api/stanja");
         			curl_setopt($zahtev, CURLOPT_RETURNTRANSFER, 1);
         			$odgovor = curl_exec($zahtev);
         			$stanje = json_decode($odgovor);
         			curl_close($zahtev);

                  foreach($stanje as $s){
               ?>
               <tr <?php
                  $kol = $s->kolicina;
                  $mkol = $s->minimalnaKolicina;
                  $okol = $s->optimalnaKolicina;
                  if($kol < $mkol){
                    echo 'class="crveno"';
                  }
                  if($kol > $okol){
                    echo 'class="zeleno"';
                  }
                ?>>
                 <td><?php echo $s->nazivProdavnice; ?> </td>
                 <td><?php echo $s->naziv; ?> </td>
                 <td><?php echo $s->nazivKategorije; ?> </td>
                 <td ><?php echo $s->kolicina; ?> </td>
               </tr>

             <?php  } ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </section>


  <?php include 'footer.php'; ?>
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/morphext/morphext.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/stickyjs/sticky.js"></script>
  <script src="lib/easing/easing.js"></script>

  <script src="js/custom.js"></script>

</body>
</html>
