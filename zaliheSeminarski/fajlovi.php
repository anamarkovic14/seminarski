<?php include 'konekcija.php';
if($_SESSION['user'] == ''){
  header("Location:login.php");
  exit;
}
 ?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Zalihe proizvoda Beograd </title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <link href="favicon.ico" rel="shortcut icon">

  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Raleway:300,400,500,700,800" rel="stylesheet">

  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/animate-css/animate.min.css" rel="stylesheet">

  <link href="css/style.css" rel="stylesheet">
</head>

<body>
  <div id="preloader"></div>

  <?php include 'header.php'; ?>

  <section id="about">
    <div class="container wow fadeInUp">
      <div class="row">
        <div class="col-md-12">
          <h3 class="section-title">Fajlovi</h3>
          <div class="section-title-divider"></div>
        </div>
      </div>
    </div>
    <div class="container wow fadeInUp">
      <?php

         $files = glob("fajlovi/*.*");
         foreach ($files as $file) {
           ?>
           <div class="col-md-12">
                   <a  href="<?php echo $file; ?>" target="blank">
                     <?php echo substr($file, 8); ; ?>
                   </a>
             </div>

           <?php
         }
      ?>


    </div>
  </section>


  <?php include 'footer.php'; ?>
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/morphext/morphext.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/stickyjs/sticky.js"></script>
  <script src="lib/easing/easing.js"></script>

  <script src="js/custom.js"></script>
  <script>

      		function popuniTabelu(){
            $('#tabela').html("");
      			var kategorija = $("#selKategorija").val();
      			$.ajax({
      				url: "ajaxPretraga.php",
      				data: "id="+kategorija,
      				success: function(result){
      				var text = '<table class="table table-hover"><thead><tr><th>Naziv</th><th>Opis</th><th>Cena</th><th>Kategorija</th><th>Proizvodjac</th></tr></thead><tbody>';
      				$.each($.parseJSON(result), function(i, val) {
      					text += '<tr>';
      					text += '<td>'+val.naziv+'</td>';
      					text += '<td>'+val.opis+'</td>';
      					text += '<td>'+val.cena+'</td>';
                text += '<td>'+val.nazivKategorije+'</td>';
                text += '<td>'+val.proizvodjac+'</td>';
      					text += '</tr>';

      					});

      					text+='</tbody></table>';
      					$('#tabela').html(text);
      			}});
      		}

      </script>
      <script>
      		$( document ).ready(function() {
      			popuniTabelu();
      		});
      </script>


</body>
</html>
