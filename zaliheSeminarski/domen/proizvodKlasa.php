<?php

class Proizvod {

	private $db;

	public function __construct($db) {
		$this->db = $db;
	}

	public function unesiProizvod() {
		if(!isset($_POST['naziv']) || !isset($_POST['opis']) || !isset($_POST['proizvodjac']) || !isset($_POST['kategorija']) || !isset($_POST['cena'])){
			return false;

		}
		if($_POST['naziv']=='' || $_POST['opis']=='' || $_POST['proizvodjac']=='' || $_POST['kategorija']=='' || $_POST['cena']==''){
			return false;

		}



			$data = Array (
					"kategorijaID" => trim($_POST['kategorija']),
					"naziv" => trim($_POST['naziv']),
					"opis" => trim($_POST['opis']),
					"proizvodjac" => trim($_POST['proizvodjac']),
					"cena" => trim($_POST['cena'])
			);

			$sacuvano = $this->db->insert('proizvod', $data);

			if($sacuvano) {
			return true;

			}
			else {
				return false;
			}



	}


}

?>
