<header id="header">
  <div class="container">

    <div id="logo" class="pull-left">
      <h1><a href="index.php">Zalihe</a></h1>
    </div>

    <nav id="nav-menu-container">
      <ul class="nav-menu">
        <li><a href="index.php">Početna</a></li>
        <li><a href="pregledStanja.php">PregledStanja</a></li>

        <li><a href="slike.php">Slike sminke</a></li>
        <?php if($_SESSION['user'] != ''){ ?>
        <li><a href="dodajStanje.php">Dodaj na stanje</a></li>
        <?php if($_SESSION['user']['rola'] == 1){ ?>
        <li><a href="izmenaBrisanjeStanja.php">Izmena i brisanje</a></li>
        <li><a href="dodajProizvod.php">Dodaj proizvod</a></li>
      <?php } ?>
      <li><a href="fajlovi.php">Fajlovi</a></li>
      <li><a href="logout.php">Logout</a></li>
    <?php }else{ ?>
      <li><a href="registracija.php">Registracija</a></li>
      <li><a href="login.php">Login</a></li>
    <?php } ?>
      </ul>
    </nav>
  </div>
</header>
