-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2018 at 08:29 PM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zaliheseminarski`
--

-- --------------------------------------------------------

--
-- Table structure for table `kategorija`
--

CREATE TABLE `kategorija` (
  `kategorijaID` int(11) NOT NULL,
  `nazivKategorije` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `minimalnaKolicina` int(11) NOT NULL,
  `optimalnaKolicina` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `kategorija`
--

INSERT INTO `kategorija` (`kategorijaID`, `nazivKategorije`, `minimalnaKolicina`, `optimalnaKolicina`) VALUES
(1, 'A', 10, 50),
(2, 'B', 5, 20),
(3, 'C', 2, 10);

-- --------------------------------------------------------

--
-- Table structure for table `prodavnica`
--

CREATE TABLE `prodavnica` (
  `prodavnicaID` int(11) NOT NULL,
  `nazivProdavnice` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adresa` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `prodavnica`
--

INSERT INTO `prodavnica` (`prodavnicaID`, `nazivProdavnice`, `adresa`) VALUES
(1, 'Artico doo', 'Maršala Tita 1'),
(2, 'Nilari parfimerija', 'Trgovačka 30b'),
(3, 'Stanković str', 'Ratka Mitrovića 183/2'),
(4, 'Dona parfimerija', 'Bulevar Despota Stefana 70'),
(5, 'Lady Line salon lepote', 'Bulevar Kralja Aleksandra 448/a lok.18'),
(6, 'Kristabel tr.', 'Terazije 30-32'),
(7, 'Anaži doo', 'Vidikovački venac 67/a');

-- --------------------------------------------------------

--
-- Table structure for table `proizvod`
--

CREATE TABLE `proizvod` (
  `proizvodID` int(11) NOT NULL,
  `naziv` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cena` double NOT NULL,
  `opis` varchar(2000) COLLATE utf8_unicode_ci NOT NULL,
  `proizvodjac` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `kategorijaID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `proizvod`
--

INSERT INTO `proizvod` (`proizvodID`, `naziv`, `cena`, `opis`, `proizvodjac`, `kategorijaID`) VALUES
(1, 'Ajlajner INGRID No Limits Beauty', 460, 'Ukoliko želite da istaknete svoj pogled, učinite to sa crnim ajlajnerom INGRID No Limits Beauty.', 'Ingrid cosmetics\r\n', 1),
(2, 'Balzam za usne INGRID Beauty Balm (Bubble Gum)', 140, 'Ukoliko maštate o balzamu za usne koji poseduje efekat zaštite, a ujedno ima i neodoljivu, trendy aromu, isprobajte balzam za usne INGRID Beauty Balm sa aromom žvakaće gume.', 'Ingrid cosmetics', 1),
(3, 'Bronzer INGRID HD Beauty Innovation', 700, 'Ukoliko želite da vaše lice bude savršeno našminkano, bronzer INGRID HD Beauty Innovation pruža vam završni make-up o kakvom ste oduvek maštali.', 'Ingrid cosmetics', 2),
(4, 'Paleta senki za oči ELIXIR Matte 40/1', 2100, 'Sada možete imati sve mat nijanse koje su vam potrebne u samo jednoj paleti. Paleta senki ELIXIR Matte 40/1 obezbeđuje vam atraktivan make-up očiju o kakvom ste oduvek maštali.', 'Elixir cosmetics', 3),
(5, 'Lutkarska sminka', 3000, 'Najbolja sminka za lutkare u gradu', 'Maratuska design', 3);

-- --------------------------------------------------------

--
-- Table structure for table `stanje`
--

CREATE TABLE `stanje` (
  `stanjeID` int(11) NOT NULL,
  `prodavnicaID` int(11) NOT NULL,
  `proizvodID` int(11) NOT NULL,
  `kolicina` int(11) NOT NULL,
  `vreme` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `stanje`
--

INSERT INTO `stanje` (`stanjeID`, `prodavnicaID`, `proizvodID`, `kolicina`, `vreme`) VALUES
(1, 1, 2, 6, '2017-11-04 08:27:48'),
(2, 7, 4, 22, '2017-11-04 08:54:37'),
(4, 2, 1, 35, '2017-11-04 09:26:29'),
(6, 6, 5, 23, '2018-01-21 16:21:23'),
(7, 4, 2, 43, '2018-01-21 16:43:10'),
(8, 5, 4, 12, '2018-01-21 16:43:16'),
(9, 7, 2, 43, '2018-01-21 16:43:21'),
(10, 3, 3, 12, '2018-01-21 16:43:27'),
(11, 5, 1, 3, '2018-01-21 16:43:56'),
(12, 3, 5, 4, '2018-01-21 16:44:02'),
(14, 1, 1, 44, '2018-01-27 18:20:13'),
(15, 5, 3, 55, '2018-02-17 17:20:35');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userID` int(11) NOT NULL,
  `imePrezime` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rola` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userID`, `imePrezime`, `username`, `password`, `rola`) VALUES
(1, 'Ana Markovic', 'ana', 'ana', 1),
(2, 'Obican Korisnik', 'korisnik', 'korisnik', 0),
(3, 'test', 'test', 'test', 0),
(4, 'testapi', 'api', 'api', 0),
(5, 'testAPI2', 'api2', 'api2', 0),
(6, 'testapi3', 'api3', 'api3', 0),
(7, 'proba', 'asdfg', '7p@?AToZ1BWL6.z', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kategorija`
--
ALTER TABLE `kategorija`
  ADD PRIMARY KEY (`kategorijaID`);

--
-- Indexes for table `prodavnica`
--
ALTER TABLE `prodavnica`
  ADD PRIMARY KEY (`prodavnicaID`);

--
-- Indexes for table `proizvod`
--
ALTER TABLE `proizvod`
  ADD PRIMARY KEY (`proizvodID`),
  ADD KEY `kategorijaID` (`kategorijaID`);

--
-- Indexes for table `stanje`
--
ALTER TABLE `stanje`
  ADD PRIMARY KEY (`stanjeID`),
  ADD KEY `prodavnicaID` (`prodavnicaID`,`proizvodID`),
  ADD KEY `proizvodID` (`proizvodID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kategorija`
--
ALTER TABLE `kategorija`
  MODIFY `kategorijaID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `prodavnica`
--
ALTER TABLE `prodavnica`
  MODIFY `prodavnicaID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `proizvod`
--
ALTER TABLE `proizvod`
  MODIFY `proizvodID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `stanje`
--
ALTER TABLE `stanje`
  MODIFY `stanjeID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `proizvod`
--
ALTER TABLE `proizvod`
  ADD CONSTRAINT `proizvod_ibfk_1` FOREIGN KEY (`kategorijaID`) REFERENCES `kategorija` (`kategorijaID`);

--
-- Constraints for table `stanje`
--
ALTER TABLE `stanje`
  ADD CONSTRAINT `stanje_ibfk_1` FOREIGN KEY (`prodavnicaID`) REFERENCES `prodavnica` (`prodavnicaID`),
  ADD CONSTRAINT `stanje_ibfk_2` FOREIGN KEY (`proizvodID`) REFERENCES `proizvod` (`proizvodID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
